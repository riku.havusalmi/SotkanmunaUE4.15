// Fill out your copyright notice in the Description page of Project Settings.

#include "TAAVI.h"
#include "BaseCharacter.h"
#include "BaseCharacterBuff.h"


ABaseCharacterBuff::ABaseCharacterBuff() {
	PrimaryActorTick.bCanEverTick = true;
	lifetime = 5.0f;
	timeAlive = 0.0f;
}

void ABaseCharacterBuff::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	GEngine->AddOnScreenDebugMessage(-1, 0.1f, FColor::Red, FString::Printf(TEXT("Shit")));
	if (buffOwner == nullptr) {
		return;
	}
	timeAlive += DeltaTime;

	if (timeAlive >= lifetime) {
		if(buffOwner != nullptr) {

			InternalBuffEnd();
			buffOwner->RemoveBuff(this);
			Destroy();
		}
	}
}

void ABaseCharacterBuff::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	if (this->GetOwner() != nullptr) {
		buffOwner = Cast<ABaseCharacter>(this->GetOwner());
		buffOwner->AddBuff(this);
		InternalBuffStart();
	}
}

void ABaseCharacterBuff::InternalBuffStart()
{
	OnBuffStart();
}

void ABaseCharacterBuff::InternalBuffEnd()
{
	OnBuffEnd();
}
