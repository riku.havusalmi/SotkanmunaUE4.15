// Fill out your copyright notice in the Description page of Project Settings.

#include "TAAVI.h"
#include "BaseWeapon.h"


// Sets default values
ABaseWeapon::ABaseWeapon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	m_CurrentState = EWeaponState::Idle;
	m_WantsToPrimaryFire = false;
	m_WantsToSecondaryFire = false;
	m_WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>("Weapon Mesh");
	m_WeaponMesh->bReceivesDecals = false;
	SetActorEnableCollision(false);
	PrimaryActorTick.bStartWithTickEnabled = true;
	PrimaryActorTick.bAllowTickOnDedicatedServer = true;
}

// Called when the game starts or when spawned
void ABaseWeapon::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickEnabled(false);
}

// Called every frame
void ABaseWeapon::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
	//GEngine->AddOnScreenDebugMessage(-1, 0.1f, FColor::Red, FString::Printf(TEXT("supertik")));
}

void ABaseWeapon::OnPrimaryFireStart()
{
	if(!m_WantsToSecondaryFire)
	{
		m_WantsToPrimaryFire = true;
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, FString::Printf(TEXT("want to fire")));
	}
}

void ABaseWeapon::OnPrimaryFireEnd()
{
	m_WantsToPrimaryFire = false;
}

void ABaseWeapon::OnSecondaryFireStart()
{
	if (!m_WantsToPrimaryFire)
	{
		m_WantsToSecondaryFire = true;
	}
}

void ABaseWeapon::OnSecondaryFireEnd()
{
	m_WantsToSecondaryFire = false;
}

void ABaseWeapon::OnPrimaryFire()
{

}

void ABaseWeapon::OnSecondaryFire()
{

}

void ABaseWeapon::OnEquip()
{
	m_CurrentState = EWeaponState::Idle;
	SetActorHiddenInGame(false);
	//PrimaryActorTick.bCanEverTick = true;
	SetActorTickEnabled(true);
}

void ABaseWeapon::OnUnEquip()
{
	SetActorHiddenInGame(true);
	//PrimaryActorTick.bCanEverTick = false;
	SetActorTickEnabled(false);
}

APlayerCharacter* ABaseWeapon::GetPlayer()
{
	AActor* owner = GetAttachParentActor();

	if (owner != nullptr)
	{
		APlayerCharacter* player = Cast<APlayerCharacter>(owner);
		if (player != nullptr)
		{
			return player;
		}
		else
		{
			return nullptr;
		}
	}
	else
	{
		return nullptr;
	}
}

