// Fill out your copyright notice in the Description page of Project Settings.

#include "TAAVI.h"
#include "Weapon_Shotgun.h"




void AWeapon_Shotgun::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	///GEngine->AddOnScreenDebugMessage(-1, 0.1f, FColor::Red, FString::Printf(TEXT("tik")));
	if (m_WantsToPrimaryFire)
	{
		if (ShotReady(m_LeftBarrelLastFired))
		{
			Shoot(true);
			m_LeftBarrelLastFired = GetWorld()->GetTimeSeconds();
			//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, FString::Printf(TEXT("Boum")));
		}
	}
	if (m_WantsToSecondaryFire)
	{
		if (ShotReady(m_RightBarrelLastFired))
		{
			Shoot(false);
			m_RightBarrelLastFired = GetWorld()->GetTimeSeconds();
		}
	}
}

void AWeapon_Shotgun::BeginPlay()
{
	Super::BeginPlay();
	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, FString::Printf(TEXT("Beginplay")));
	PrimaryActorTick.bStartWithTickEnabled = true;
	PrimaryActorTick.bAllowTickOnDedicatedServer = true;
}

void AWeapon_Shotgun::ShootFrom(FVector point)
{

}

void AWeapon_Shotgun::Shoot(bool leftBarrel)
{
	UGameplayStatics::PlaySound2D(GetWorld(), m_FireSound);
	UCameraComponent* cam = GetPlayer()->FindComponentByClass<UCameraComponent>();
	if (cam != nullptr)
	{
		FVector traceStart = cam->GetComponentLocation();
		FVector traceEnd;
		for (int Index = 0; Index < m_NumOfBullets ; Index++)
		{
			traceEnd = traceStart;

			FRotator rot = cam->GetForwardVector().Rotation();
			traceEnd += rot.RotateVector(FMath::VRandCone(FVector(1.0f, 0.0f, 0.0f), FMath::DegreesToRadians(m_Spread / 2)))*m_MaxDistance;

			FCollisionQueryParams traceParams(FName(TEXT("ShotgunTrace")), true, this);
			traceParams.AddIgnoredActor(GetPlayer());
			traceParams.bReturnFaceIndex = true;
			FHitResult result(ForceInit);
			

			if (GetWorld()->LineTraceSingleByChannel(result, traceStart, traceEnd, ECC_Visibility, traceParams))
			{
				DrawDebugLine(GetWorld(), traceStart, result.ImpactPoint, FColor::Green, false, 2.0f,0,0.5f);
				if (result.Actor != nullptr)
				{
					float dist = FMath::Clamp((traceStart - result.ImpactPoint).Size(),0.0f,m_MaxDistance);
					float damage = m_DamageOverDistanceCurve->GetFloatValue(dist / m_MaxDistance)/m_NumOfBullets;
					//UGameplayStatics::ApplyPointDamage((TWeakPtr<AActor>)result.Actor, m_PrimaryDamage, traceStart, result, GetPlayer()->GetInstigatorController(), GetPlayer(), m_DamageType);
					FPointDamageEvent dmgEvent(damage*m_PrimaryDamage,result,traceStart,m_DamageType->StaticClass());
					result.Actor->TakeDamage(damage*m_PrimaryDamage, dmgEvent, GetPlayer()->GetInstigatorController(), GetPlayer());
				}
			}
			else {
				DrawDebugLine(GetWorld(), traceStart, traceEnd, FColor::Yellow, false, 2.0f, 0, 0.5f);
			}
		}
	}
}
