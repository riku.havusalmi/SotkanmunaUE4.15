// Fill out your copyright notice in the Description page of Project Settings.

#include "TAAVI.h"
#include "PredictionTarget.h"


// Sets default values
APredictionTarget::APredictionTarget()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	this->GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	this->GetCapsuleComponent()->SetEnableGravity(false);
	this->SetActorEnableCollision(false);
	this->GetCapsuleComponent()->SetCapsuleHalfHeight(20);
}


