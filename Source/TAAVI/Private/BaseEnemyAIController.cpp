// Fill out your copyright notice in the Description page of Project Settings.

#include "TAAVI.h"
#include "BaseEnemyAIController.h"
#include "BaseEnemy.h"
#include "Perception/AIPerceptionComponent.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Bool.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Object.h"



void ABaseEnemyAIController::Possess(APawn* InPawn)
{
	Super::Possess(InPawn);
	ABaseEnemy* Enemy = Cast<ABaseEnemy>(InPawn);
	aiOwner = Enemy;
	Enemy->SetAiController(this);
	myPerception->OnPerceptionUpdated.AddDynamic(this, &ABaseEnemyAIController::UpdatePerception);

	// start behavior
	if (Enemy && Enemy->EnemyBehavior)
	{
		if (Enemy->EnemyBehavior->BlackboardAsset)
		{
			BlackboardComp->InitializeBlackboard(*Enemy->EnemyBehavior->BlackboardAsset);
		}

		AggroTargetID = BlackboardComp->GetKeyID("AggroTarget");
		FleeFromID = BlackboardComp->GetKeyID("FleeFrom");
		IsDeadID = BlackboardComp->GetKeyID("IsDead");
		IsOnFireID = BlackboardComp->GetKeyID("IsOnFire");

		BehaviorComp->StartTree(*(Enemy->EnemyBehavior));
	}
	
}

void ABaseEnemyAIController::UnPossess()
{
	Super::UnPossess();

	BehaviorComp->StopTree();

	if (aiOwner != nullptr) {
		aiOwner->SetAiController(nullptr);
		aiOwner = nullptr;
	}
}

ABaseCharacter* ABaseEnemyAIController::GetAggroTarget()
{
	ABaseCharacter* character = Cast<ABaseCharacter>(BlackboardComp->GetValue<UBlackboardKeyType_Object>(AggroTargetID));
	return character;
}

void ABaseEnemyAIController::SetAggroTarget(ABaseCharacter* newTarget)
{
	BlackboardComp->SetValue<UBlackboardKeyType_Object>(AggroTargetID, newTarget);
}

ABaseEnemyAIController::ABaseEnemyAIController(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	// Try to set blackboard shits 
	BlackboardComp = ObjectInitializer.CreateDefaultSubobject<UBlackboardComponent>(this, TEXT("BlackboardComp"));
	BrainComponent = BehaviorComp = ObjectInitializer.CreateDefaultSubobject<UBehaviorTreeComponent>(this, TEXT("BehaviorComp"));

	//myPerception = GetAIPerceptionComponent();
	myPerception = CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("AIPerception"));
	mySightSense = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("Sight sense config"));
	mySightSense->DetectionByAffiliation.bDetectEnemies = true;
	mySightSense->DetectionByAffiliation.bDetectFriendlies = true;
	mySightSense->DetectionByAffiliation.bDetectNeutrals = true;
	myPerception->ConfigureSense(*mySightSense);
}

void ABaseEnemyAIController::UpdatePerception(TArray<AActor*> updatedActors)
{
	if(BlackboardComp->GetValue<UBlackboardKeyType_Object>(AggroTargetID) == nullptr)
	{
		for (int x = 0; x < updatedActors.Num(); x++)
		{
			ABaseCharacter* character = Cast<ABaseCharacter>(updatedActors[x]);
			if (character != nullptr && character->GetCharacterName() == "Taavi") {
				BlackboardComp->SetValue<UBlackboardKeyType_Object>(AggroTargetID, character);
				GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, FString::Printf(TEXT("TAAVI SPOTTED")));
			}
		}
	}
	//myPerception->GetActorsPerception()
}
