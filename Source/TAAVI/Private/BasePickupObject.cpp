// Fill out your copyright notice in the Description page of Project Settings.

#include "TAAVI.h"
#include "BasePickupObject.h"


// Sets default values
ABasePickupObject::ABasePickupObject(const FObjectInitializer& ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	triggerCollision = ObjectInitializer.CreateDefaultSubobject<USphereComponent>(this, "TriggerCollider");
	triggerCollision->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	triggerCollision->SetCollisionResponseToAllChannels(ECR_Ignore);
	triggerCollision->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);

	RootComponent = triggerCollision;
}

// Called when the game starts or when spawned
void ABasePickupObject::BeginPlay()
{
	Super::BeginPlay();
}

void ABasePickupObject::NotifyActorBeginOverlap(AActor* OtherActor)
{
	ABaseCharacter* character = Cast<ABaseCharacter>(OtherActor);

	if(character!=nullptr)
	{
		UGameplayStatics::PlaySound2D(GetWorld(), pickupSound);
		InternalPerformPickup(character);
		Destroy();
	}
}

void ABasePickupObject::InternalPerformPickup(ABaseCharacter* character)
{
	PerformPickupEvent(character);
}
