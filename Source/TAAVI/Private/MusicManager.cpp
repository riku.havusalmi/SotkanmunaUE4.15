// Fill out your copyright notice in the Description page of Project Settings.

#include "TAAVI.h"
#include "MusicManager.h"


// Sets default values
AMusicManager::AMusicManager(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	AudioComp = ObjectInitializer.CreateDefaultSubobject<UAudioComponent>(this, TEXT("AudioComp"));
	RootComponent = AudioComp;

	InCombat = false;

}

// Called when the game starts or when spawned
void AMusicManager::BeginPlay()
{
	Super::BeginPlay();
	UE_LOG(LogTemp, Warning, TEXT("Music manager created."));
	UE_LOG(LogTemp, Warning, TEXT("Num of entries: %i"), MusicArray.Num());
	
}

// Called every frame
void AMusicManager::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void AMusicManager::SetIsInCombat(bool _inCombat) 
{
	if (_inCombat == InCombat)
		return;

	InCombat = _inCombat;
	UE_LOG(LogTemp, Warning, TEXT("Num of entries: %i"), MusicArray.Num());
	if (InCombat) 
	{
		AudioComp->SetSound(FindRandomTrack(MTYPE_Combat));
		AudioComp->Play();
		UE_LOG(LogTemp, Warning, TEXT("Combat music should be playing."));
	}
	else
	{
		AudioComp->Stop();
	}
}

USoundCue* AMusicManager::FindRandomTrack(EMusicType type) 
{
	TArray<USoundCue*> tracksOfType;

	for (int i = 0; i < MusicArray.Num(); i++) 
	{
		if (MusicArray[i].MusicType == type) 
		{
			tracksOfType.Add(MusicArray[i].MusicCue);
		}
	}

	if (tracksOfType.Num() == 0) 
	{
		return 0;
	}
	else 
	{
		return tracksOfType[FMath::RandRange(0, tracksOfType.Num() - 1)];
	}
}
