// Fill out your copyright notice in the Description page of Project Settings.

#include "TAAVI.h"
#include "BaseEnemy.h"
#include "PredictionTarget.h"
#include "BaseEnemyAIController.h"


ABaseEnemy::ABaseEnemy()
{
	AIControllerClass = ABaseEnemyAIController::StaticClass(); 
	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;
	_predictionTarget = nullptr;
}

void ABaseEnemy::SetFocusPoint(FVector loc)
{
	if (_predictionTarget == nullptr) {
		_predictionTarget = GetWorld()->SpawnActor<APredictionTarget>();
	}

	if(AI_FIXAIMBUG && _predictionTarget != nullptr)
	{
		_predictionTarget->SetActorLocation(loc);
		_aiController->SetFocus(_predictionTarget);
	}
	else
	{
		_aiController->SetFocalPoint(loc);
	}
}

void ABaseEnemy::_destroyPredictionTarget()
{
	if(AI_FIXAIMBUG && _predictionTarget != nullptr)
	{
		_predictionTarget->Destroy();
	}
}

void ABaseEnemy::InternalOnDeath()
{
	Super::InternalOnDeath();
}
