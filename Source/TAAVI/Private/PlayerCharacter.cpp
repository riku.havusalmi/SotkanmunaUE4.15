// Fill out your copyright notice in the Description page of Project Settings.

#include "TAAVI.h"
#include "PlayerCharacter.h"




APlayerCharacter::APlayerCharacter()
{
	PrimaryActorTick.bCanEverTick = true;
	JumpMaxCount = 2;
	JumpCurrentCount = 0;
	m_Jumps = 0;
	AllowedToClimb = false;
	ClimbingEdge = false;
}

void APlayerCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	if (AllowedToClimb) {
		TryLedgeGrab();
	}
}

void APlayerCharacter::HandleJump()
{
	if (ClimbingEdge) {
		return;
	}
	if (m_Jumps == 0 || (GetCharacterMovement()->MovementMode == MOVE_Falling && m_Jumps > 0 && m_Jumps < JumpMaxCount))
	{
		Jump();

		if (m_Jumps != 0 && (m_ForwardBackward != 0.0f || m_RightLeft != 0.0f))
		{
			DoAirDash();
		}
	}
	else
	{
		return;
	}
	m_Jumps++;
}

void APlayerCharacter::Landed(const FHitResult& Hit)
{
	Super::Landed(Hit);
	m_Jumps = 0;
}

void APlayerCharacter::FinishClimbing()
{
	GetCharacterMovement()->SetMovementMode(MOVE_Walking);
	m_Jumps = 0;
	ClimbingEdge = false;
}

void APlayerCharacter::DoAirDash()
{
	float currentVelocityMagnitude = GetCharacterMovement()->Velocity.Size();

	FVector rightVector = GetCapsuleComponent()->GetRightVector();
	FVector forwardVector = GetCapsuleComponent()->GetForwardVector();

	GetCharacterMovement()->Velocity = 
		FMath::Lerp((forwardVector*currentVelocityMagnitude)*m_ForwardBackward, (rightVector*currentVelocityMagnitude)*m_RightLeft, FMath::Abs(m_RightLeft));
}

void APlayerCharacter::TryLedgeGrab() {
	if (AllowedToClimb && !ClimbingEdge)
	{
		FVector traceStart = GetActorLocation();
		FVector traceEnd = GetActorForwardVector();
		traceEnd.X *= 150.0f;
		traceEnd.Y *= 150.0f;
		traceEnd += traceStart;

		FCollisionQueryParams traceParams(FName(TEXT("LedgeTrace")), false, this);
		FHitResult result(ForceInit);

		FVector wallTracePoint;
		FVector wallTraceNormal;

		if (GetWorld()->LineTraceSingleByChannel(result, traceStart, traceEnd, ECC_GameTraceChannel1, traceParams))
		{
			wallTracePoint = result.ImpactPoint;
			wallTraceNormal = result.ImpactNormal;


			traceStart = wallTraceNormal*-20.0f;
			traceStart += wallTracePoint;
			traceStart.Z += 500.0f;

			traceEnd = traceStart;
			traceEnd.Z -= 550.0f;

			if (GetWorld()->LineTraceSingleByChannel(result, traceStart, traceEnd, ECC_GameTraceChannel1, traceParams))
			{

				FVector ledgeHeight = result.ImpactPoint;
				ClimbingEdge = true;
				GetCharacterMovement()->SetMovementMode(MOVE_Flying);

				float radius = GetCapsuleComponent()->GetScaledCapsuleRadius() - 9.0f;

				FVector climbTo = wallTraceNormal;
				climbTo.X *= radius;
				climbTo.Y *= radius;
				climbTo += wallTracePoint;
				climbTo.Z = ledgeHeight.Z - 65.0f;

				FRotator climbRot = GetActorRotation();
				climbRot.Yaw = FRotationMatrix::MakeFromX(GetActorLocation() - climbTo).Rotator().Yaw;

				PerformClimb(climbTo, climbRot);
			}
		}
	}
}
