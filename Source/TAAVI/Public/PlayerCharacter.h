// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BaseCharacter.h"
#include "PlayerCharacter.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class TAAVI_API APlayerCharacter : public ABaseCharacter
{
	GENERATED_BODY()
	
public:	
	APlayerCharacter();
	
	// Called every frame
	virtual void Tick(float DeltaSeconds) override;
	
	UFUNCTION(BlueprintCallable, Category = "Movement")
	void HandleJump();

	virtual void Landed(const FHitResult& Hit) override;

	UPROPERTY(BlueprintReadWrite, Category = "Movement")
	int m_Jumps;
	UPROPERTY(BlueprintReadWrite, Category = "Movement")
	float m_ForwardBackward;
	UPROPERTY(BlueprintReadWrite, Category = "Movement")
	float m_RightLeft;

	UFUNCTION(BlueprintImplementableEvent, Category = "Climbing")
	void PerformClimb(FVector climbTo, FRotator climbRot);

	UFUNCTION(BlueprintCallable, Category = "Climbing")
	void FinishClimbing();

	UPROPERTY(BlueprintReadWrite, Category = "Climbing")
	bool AllowedToClimb;

	UPROPERTY(BlueprintReadWrite, Category = "Climbing")
	bool ClimbingEdge;

private:

	void DoAirDash();

	void TryLedgeGrab();
};
