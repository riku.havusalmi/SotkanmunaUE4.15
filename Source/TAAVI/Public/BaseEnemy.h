// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BaseCharacter.h"
#include "BaseEnemy.generated.h"

/**
 * 
 */

const bool AI_FIXAIMBUG = true;

class APredictionTarget;
class ABaseEnemyAIController;

UCLASS(Blueprintable)
class TAAVI_API ABaseEnemy : public ABaseCharacter
{
	GENERATED_BODY()

public:

	ABaseEnemy();

	UFUNCTION(BlueprintCallable, Category = "Base enemy")
	void SetFocusPoint(FVector loc);

	UFUNCTION(BlueprintCallable, Category = "Base enemy")
	ABaseEnemyAIController* GetAIController() {
		return _aiController;
	}

	UFUNCTION(BlueprintCallable, Category = "Base enemy")
	bool GetIsOnFire() const { return isOnFire; }

	UFUNCTION(BlueprintCallable, Category = "Base enemy")
	void SetOnFire(bool val) { isOnFire = val; }

	UPROPERTY(EditAnywhere, Category = "Enemy AI")
	class UBehaviorTree* EnemyBehavior;


	void SetAiController(ABaseEnemyAIController* val) { _aiController = val; }
private:
	APredictionTarget* _predictionTarget;
	ABaseEnemyAIController* _aiController;

	UFUNCTION()
	void _destroyPredictionTarget();

protected:

	virtual void InternalOnDeath() override;

	UPROPERTY(BlueprintReadOnly, Category = "Enemy attributes")
	float courage;

	UPROPERTY(BlueprintReadOnly, Category = "Enemy attributes")
	bool canBeSetOnFire;

	UPROPERTY(BlueprintReadOnly, Category = "Enemy status")
	bool isOnFire;
	
	UPROPERTY(BlueprintReadOnly, Category = "Enemy status")
	bool isRagdoll;
};
