// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "BaseCharacterBuff.generated.h"

/**
 * 
 */
class ABaseCharacter;
UCLASS(Blueprintable)
class TAAVI_API ABaseCharacterBuff : public AActor
{
	GENERATED_BODY()

public:
	ABaseCharacterBuff();

	UPROPERTY(BlueprintReadOnly)
	ABaseCharacter* buffOwner;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	virtual void PostInitializeComponents() override;


	UFUNCTION(BlueprintImplementableEvent)
	void OnBuffStart();

	UFUNCTION(BlueprintImplementableEvent)
	void OnBuffEnd();
	
protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float lifetime;
	
	UPROPERTY(BlueprintReadOnly)
	float timeAlive;

private:
	void InternalBuffStart();
	void InternalBuffEnd();
};