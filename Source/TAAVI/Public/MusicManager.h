// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "MusicManager.generated.h"


UENUM(BlueprintType)
enum EMusicType
{
	MTYPE_Idle UMETA(DisplayName = "Idle"),
	MTYPE_Combat UMETA(DisplayName = "Combat")
};

USTRUCT(BlueprintType)
struct FBGMusic
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Music Manager")
	TEnumAsByte<EMusicType> MusicType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Music Manager")
	USoundCue* MusicCue;

};

UCLASS()
class TAAVI_API AMusicManager : public AActor
{
	GENERATED_UCLASS_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Music Manager")
	UAudioComponent* AudioComp;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Music Manager")
	bool InCombat;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Music Manager")
	TArray<FBGMusic> MusicArray;
	
public:	
	// Sets default values for this actor's properties
	AMusicManager();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UFUNCTION(BlueprintCallable, Category = "Music Manager")
	void SetIsInCombat(bool _inCombat);

private:
	USoundCue* FindRandomTrack(EMusicType type);
	
};
