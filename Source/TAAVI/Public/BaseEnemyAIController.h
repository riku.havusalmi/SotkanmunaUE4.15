// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "Perception/AISenseConfig_Sight.h"
#include "Perception/AISenseConfig_Hearing.h"
#include "Perception/AISenseConfig_Damage.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BaseEnemyAIController.generated.h"

/**
 * 
 */

class UAIPerceptionComponent;
class ABaseEnemy;

UCLASS(Blueprintable)
class TAAVI_API ABaseEnemyAIController : public AAIController
{
	GENERATED_BODY()


public: 
	virtual void Possess(class APawn* InPawn) override;
	virtual void UnPossess() override;
	
	UFUNCTION(BlueprintCallable, Category = "Enemy AI")
	ABaseCharacter* GetAggroTarget();

	UFUNCTION(BlueprintCallable, Category = "Enemy AI")
	void SetAggroTarget(ABaseCharacter* newTarget);
	
	ABaseEnemyAIController(const FObjectInitializer& ObjectInitializer);

protected:
	UPROPERTY(Transient)
	UBlackboardComponent* BlackboardComp;

	/* Cached BT component */
	UPROPERTY(Transient)
	UBehaviorTreeComponent* BehaviorComp;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Enemy AI")
	UAIPerceptionComponent* myPerception;

	int32 AggroTargetID;
	int32 FleeFromID;
	int32 IsDeadID;
	int32 IsOnFireID;

	UAISenseConfig_Sight* mySightSense;

	UFUNCTION()
	void UpdatePerception(TArray<AActor*> updatedActors);

private:
	ABaseEnemy* aiOwner;
};
