// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "BaseCharacter.h"
#include "BasePickupObject.generated.h"

UCLASS(Blueprintable)
class TAAVI_API ABasePickupObject : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABasePickupObject(const FObjectInitializer& ObjectInitializer);

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

	UPROPERTY(EditDefaultsOnly)
	USphereComponent* triggerCollision;

	UPROPERTY(EditDefaultsOnly)
	USoundBase* pickupSound;

private:
	void InternalPerformPickup(ABaseCharacter* character);

protected:
	UFUNCTION(BlueprintImplementableEvent)
	void PerformPickupEvent(ABaseCharacter* character);
};
