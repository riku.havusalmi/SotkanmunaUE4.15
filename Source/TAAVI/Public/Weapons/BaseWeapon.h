// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "PlayerCharacter.h"
#include "BaseWeapon.generated.h"

namespace EWeaponState
{
	enum Type
	{
		Idle,
		FiringPrimary,
		FiringSecondary,
		Reloading,
		Equipping,
	};
}

UCLASS(Abstract, Blueprintable)
class TAAVI_API ABaseWeapon : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABaseWeapon();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	UFUNCTION(BlueprintCallable, Category = "Base weapon events")
	virtual void OnPrimaryFireStart();

	UFUNCTION(BlueprintCallable, Category = "Base weapon events")
	virtual void OnPrimaryFireEnd();

	UFUNCTION(BlueprintCallable, Category = "Base weapon events")
	virtual void OnSecondaryFireStart();

	UFUNCTION(BlueprintCallable, Category = "Base weapon events")
	virtual void OnSecondaryFireEnd();

	UFUNCTION(BlueprintCallable, Category = "Base weapon events")
	virtual void OnPrimaryFire();

	UFUNCTION(BlueprintCallable, Category = "Base weapon events")
	virtual void OnSecondaryFire();

	UFUNCTION(BlueprintCallable, Category = "Base weapon events")
	virtual void OnEquip();

	UFUNCTION(BlueprintCallable, Category = "Base weapon events")
	virtual void OnUnEquip();

	UFUNCTION(BlueprintCallable, Category = "Base weapon")
	APlayerCharacter* GetPlayer();

	UPROPERTY(BlueprintReadOnly, VisibleDefaultsOnly, Category = "Base weapon settings")
	USkeletalMeshComponent* m_WeaponMesh;

	UFUNCTION(BlueprintCallable, Category = "Base weapon")
		USkeletalMeshComponent* getWeaponMesh() {
		return m_WeaponMesh;
	}

protected:

	EWeaponState::Type m_CurrentState;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Base weapon attributes")
	float m_PrimaryDamage;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Base weapon attributes")
	float m_SecondaryDamage;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Base weapon settings")
	FName m_WeaponAttachSocket;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Base weapon settings")
	FName m_LeftHandIKTargetSocket;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Base weapon settings")
	USoundBase* m_FireSound;

	bool m_WantsToPrimaryFire;
	bool m_WantsToSecondaryFire;

	float m_LastPrimaryFireTime;
	float m_LastSecondaryFireTime;

	FTimerHandle TimerHandle_HandlePrimaryFire;
	FTimerHandle TimerHandle_HandleSecondaryFire;

};
