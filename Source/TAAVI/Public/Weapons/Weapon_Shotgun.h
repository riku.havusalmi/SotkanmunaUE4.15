// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Weapons/BaseWeapon.h"
#include "Weapon_Shotgun.generated.h"

/**
 * 
 */
UCLASS(Abstract, Blueprintable)
class TAAVI_API AWeapon_Shotgun : public ABaseWeapon
{
	GENERATED_BODY()
	
	
public:

	AWeapon_Shotgun() {
		m_LeftBarrelLastFired = 0.0f;
		m_RightBarrelLastFired = 0.0f;
	}

	virtual void Tick(float DeltaSeconds) override;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
protected:
	UPROPERTY(BlueprintReadOnly,EditDefaultsOnly, Category = "Shotgun settings")
	UDamageType* m_DamageType;

	UPROPERTY(BlueprintReadOnly,EditDefaultsOnly, Category = "Shotgun settings")
	UCurveFloat* m_DamageOverDistanceCurve;

	UPROPERTY(BlueprintReadOnly,EditDefaultsOnly, Category = "Shotgun settings")
	float m_Spread;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Shotgun settings")
	float m_MaxDistance;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Shotgun settings")
	int32 m_NumOfBullets;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Shotgun settings")
	bool m_AutoMode;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Shotgun settings")
	float m_TimeBetweenShots;

	UPROPERTY(BlueprintReadWrite, Category = "Shotgun settings")
	FVector m_LeftBarrelPoint;

	float m_LeftBarrelLastFired;

	UPROPERTY(BlueprintReadWrite, Category = "Shotgun settings")
	FVector m_RightBarrelPoint;

	float m_RightBarrelLastFired;

	bool ShotReady(float timeStamp) {
		if (GetWorld()->GetTimeSeconds() >= timeStamp + m_TimeBetweenShots)
		{
			return true;
		}
		else
		{
			//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, FString::Printf(TEXT("false")));
			return false;
		}
	}

	void ShootFrom(FVector point);
private:
	void Shoot(bool leftBarrel);
};
