// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "PredictionTarget.generated.h"

UCLASS()
class TAAVI_API APredictionTarget : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APredictionTarget();
};
