// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "BaseCharacterBuff.h"
#include "BaseCharacter.generated.h"

UCLASS(Blueprintable)
class TAAVI_API ABaseCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ABaseCharacter();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual void PostInitializeComponents() override;

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser);

	UFUNCTION(BlueprintCallable, Category= "Base character")
	void SetMovementSpeed(float val) { 
		_currentMovementSpeed = val; 
		GetCharacterMovement()->MaxWalkSpeed = val;
	}

	UFUNCTION(BlueprintCallable, Category = "Base character")
	void SetHealth(float val) {
		_health = FMath::Clamp(val, 0.0f, _maxHealth);
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, FString::Printf(TEXT("Health = %f"), _health));
		if (_health <= 0.0f)
		{
			_isDead = true;
			InternalOnDeath();
		}
		else if (_health>0.0f && _isDead)
		{
			_isDead = false;
		}
	 }

	UFUNCTION(BlueprintCallable, Category = "Base character")
	void AddHealth(float health) {
		SetHealth(_health + health);
	}

	UFUNCTION(BlueprintImplementableEvent)
	void OnBuffAdded();

	UFUNCTION(BlueprintImplementableEvent)
	void OnBuffRemoved();

	void AddBuff(ABaseCharacterBuff* buff) {
		_myBuffs.AddUnique(buff);
		OnBuffAdded();
	}

	void RemoveBuff(ABaseCharacterBuff* buff) {
		//int index = _myBuffs.Find(buff);
		_myBuffs.Remove(buff);
		OnBuffRemoved();
	}

	FString GetCharacterName() const { return _name; }
	void SetCharacterName(FString val) { _name = val; }

	virtual void Landed(const FHitResult& Hit) override;

protected:

	virtual void InternalOnDeath();

	UFUNCTION(BlueprintImplementableEvent)
	void OnDeathEvent();

	UPROPERTY(BlueprintReadOnly, Category="Base attributes")
	float _health;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Base attributes")
	float _maxHealth;

	UPROPERTY(BlueprintReadOnly, Category = "Base attributes")
	bool _isDead;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Base attributes")
	float _walkSpeed;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Base attributes")
	float _runSpeed;

	UPROPERTY(BlueprintReadOnly, Category = "Base attributes")
	float _currentMovementSpeed;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Base attributes")
	FString _name;
	
	UPROPERTY()
	TArray<ABaseCharacterBuff*> _myBuffs;
};
