// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	C++ class header boilerplate exported from UnrealHeaderTool.
	This is automatically generated by the tools.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef VICTORYBPLIBRARY_VictoryBPHTML_generated_h
#error "VictoryBPHTML.generated.h already included, missing '#pragma once' in VictoryBPHTML.h"
#endif
#define VICTORYBPLIBRARY_VictoryBPHTML_generated_h

#define SotkanmunaUE4_15_Plugins_Runtime_VictoryPlugin_Source_VictoryBPLibrary_Public_VictoryBPHTML_h_21_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execVictoryHTML5_SetCursorVisible) \
	{ \
		P_GET_UBOOL(Z_Param_MakeVisible); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		UVictoryBPHTML::VictoryHTML5_SetCursorVisible(Z_Param_MakeVisible); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIsHTML) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=UVictoryBPHTML::IsHTML(); \
		P_NATIVE_END; \
	}


#define SotkanmunaUE4_15_Plugins_Runtime_VictoryPlugin_Source_VictoryBPLibrary_Public_VictoryBPHTML_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execVictoryHTML5_SetCursorVisible) \
	{ \
		P_GET_UBOOL(Z_Param_MakeVisible); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		UVictoryBPHTML::VictoryHTML5_SetCursorVisible(Z_Param_MakeVisible); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIsHTML) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=UVictoryBPHTML::IsHTML(); \
		P_NATIVE_END; \
	}


#define SotkanmunaUE4_15_Plugins_Runtime_VictoryPlugin_Source_VictoryBPLibrary_Public_VictoryBPHTML_h_21_INCLASS_NO_PURE_DECLS \
	private: \
	static void StaticRegisterNativesUVictoryBPHTML(); \
	friend VICTORYBPLIBRARY_API class UClass* Z_Construct_UClass_UVictoryBPHTML(); \
	public: \
	DECLARE_CLASS(UVictoryBPHTML, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/VictoryBPLibrary"), NO_API) \
	DECLARE_SERIALIZER(UVictoryBPHTML) \
	/** Indicates whether the class is compiled into the engine */ \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SotkanmunaUE4_15_Plugins_Runtime_VictoryPlugin_Source_VictoryBPLibrary_Public_VictoryBPHTML_h_21_INCLASS \
	private: \
	static void StaticRegisterNativesUVictoryBPHTML(); \
	friend VICTORYBPLIBRARY_API class UClass* Z_Construct_UClass_UVictoryBPHTML(); \
	public: \
	DECLARE_CLASS(UVictoryBPHTML, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/VictoryBPLibrary"), NO_API) \
	DECLARE_SERIALIZER(UVictoryBPHTML) \
	/** Indicates whether the class is compiled into the engine */ \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SotkanmunaUE4_15_Plugins_Runtime_VictoryPlugin_Source_VictoryBPLibrary_Public_VictoryBPHTML_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVictoryBPHTML(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVictoryBPHTML) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVictoryBPHTML); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVictoryBPHTML); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVictoryBPHTML(UVictoryBPHTML&&); \
	NO_API UVictoryBPHTML(const UVictoryBPHTML&); \
public:


#define SotkanmunaUE4_15_Plugins_Runtime_VictoryPlugin_Source_VictoryBPLibrary_Public_VictoryBPHTML_h_21_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVictoryBPHTML(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVictoryBPHTML(UVictoryBPHTML&&); \
	NO_API UVictoryBPHTML(const UVictoryBPHTML&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVictoryBPHTML); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVictoryBPHTML); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVictoryBPHTML)


#define SotkanmunaUE4_15_Plugins_Runtime_VictoryPlugin_Source_VictoryBPLibrary_Public_VictoryBPHTML_h_21_PRIVATE_PROPERTY_OFFSET
#define SotkanmunaUE4_15_Plugins_Runtime_VictoryPlugin_Source_VictoryBPLibrary_Public_VictoryBPHTML_h_18_PROLOG
#define SotkanmunaUE4_15_Plugins_Runtime_VictoryPlugin_Source_VictoryBPLibrary_Public_VictoryBPHTML_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SotkanmunaUE4_15_Plugins_Runtime_VictoryPlugin_Source_VictoryBPLibrary_Public_VictoryBPHTML_h_21_PRIVATE_PROPERTY_OFFSET \
	SotkanmunaUE4_15_Plugins_Runtime_VictoryPlugin_Source_VictoryBPLibrary_Public_VictoryBPHTML_h_21_RPC_WRAPPERS \
	SotkanmunaUE4_15_Plugins_Runtime_VictoryPlugin_Source_VictoryBPLibrary_Public_VictoryBPHTML_h_21_INCLASS \
	SotkanmunaUE4_15_Plugins_Runtime_VictoryPlugin_Source_VictoryBPLibrary_Public_VictoryBPHTML_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SotkanmunaUE4_15_Plugins_Runtime_VictoryPlugin_Source_VictoryBPLibrary_Public_VictoryBPHTML_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SotkanmunaUE4_15_Plugins_Runtime_VictoryPlugin_Source_VictoryBPLibrary_Public_VictoryBPHTML_h_21_PRIVATE_PROPERTY_OFFSET \
	SotkanmunaUE4_15_Plugins_Runtime_VictoryPlugin_Source_VictoryBPLibrary_Public_VictoryBPHTML_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	SotkanmunaUE4_15_Plugins_Runtime_VictoryPlugin_Source_VictoryBPLibrary_Public_VictoryBPHTML_h_21_INCLASS_NO_PURE_DECLS \
	SotkanmunaUE4_15_Plugins_Runtime_VictoryPlugin_Source_VictoryBPLibrary_Public_VictoryBPHTML_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SotkanmunaUE4_15_Plugins_Runtime_VictoryPlugin_Source_VictoryBPLibrary_Public_VictoryBPHTML_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
